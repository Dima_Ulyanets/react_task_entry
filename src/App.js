import React, {Component} from 'react'
import {connect} from 'react-redux'


/**
 * Main Wrapper
 */
class App extends Component {
   
    render() {
        return (
            <div className="main-wrapper">
                <label>Acceptance criteria:</label>
                <ol>
                   <li>  Form for creating comments</li>
                    <li> Simulate async request to the api with timeout function(give 1.5 second timeout).</li>
                    <li> If comment less or equal to 25 symbols then submit it.</li>
                    <li> If comment more then 25 symbols then show error</li>
                    <li> Validation should be done in simulated function(it should simulate backend validation error)</li>
                   <li>  Handle error on frontend and show error on UI.</li>
                   <li>  List of comments</li>
                   <li>  Show some indicator to user while request is pending</li>
                </ol>
            </div>
        )
    }
}

export default connect()(App);